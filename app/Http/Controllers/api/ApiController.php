<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use DB;
use Validator;

use App\Models\Customers;
use App\Models\Orders;
use App\Models\OrderDetails;
use App\Models\Products;

class ApiController extends Controller
{

    // show all orders and order details  with respect to customer
    public function showAllOrders(Request $request)
    {

        $rules = [
            'customer_id'     => 'required',
        ];
        $messages = [
            'customer_id.required'    => 'Customer is required',
        ];
        $validation = Validator::make($request->all(), $rules, $messages);
        if ($validation->fails()) {
            return response()->json(['status' => 400, 'response' => $validation->errors()->first()]);
        }
        $customer_id = $request->customer_id;
        try{
            //getting customer details
            $check_customer = Customers::where('customerNumber',$customer_id)->first();

            if($check_customer){ // checking customer exist
                //getting all orders of the customer with order details
                $orders = Orders::with('order_details')->where('customerNumber',$customer_id)->get();
                return response()->json(['status' => 200, 'response' => $orders]);
            }else{
                return response()->json(['status' => 400, 'response' => 'Invalid Customer']);
            }
        }catch(\Exception $e){
            return response()->json(['status'=>500,'response'=>'Internal Server Error']);
        }
    }

    // get top 10 customer having high purchase
    public function topCustomerPurchases(Request $request)
    {
        try{
            $customer_order = OrderDetails::select('customers.customerNumber','customers.customerName',DB::raw('COUNT(orderdetails.orderNumber) as orderItems'),DB::raw('SUM(orderdetails.quantityOrdered) as itemQuantity'))
                                    ->join('orders','orders.orderNumber','orderdetails.orderNumber')
                                    ->join('customers','customers.customerNumber','orders.customerNumber')
                                    ->groupBy('customers.customerNumber')
                                    ->groupBy('orderdetails.orderNumber')
                                    ->orderBy('orders.customerNumber','DESC')
                                    ->get();

            $top_customers = [];
            $last_customer_id = '';
            $last_customer_name = '';
            $last_customer_item = '';
            $customer_que = [];
            $quantity = 0;
            $items = 0;
            foreach($customer_order as $each_order){
                if($last_customer_id == $each_order['customerNumber']){
                    $quantity = $quantity + $each_order['itemQuantity'];
                    $items = $items + $each_order['orderItems'];
                }else{
                    if($quantity != 0){
                        $check_customer = Customers::where('customerNumber',$last_customer_id)->first();
                        $order_count = Orders::where('customerNumber',$last_customer_id)->count();
                        if($check_customer){
                            $customer_que[] = array(
                                'customerNumber' => $last_customer_id,
                                'customerName'   => $last_customer_name,
                                'orderCount'     => $order_count,
                                'orderItems'     => $items,
                                'itemQuantity'   => $quantity,
                            );
                        }
                    }
                    $quantity = $each_order['itemQuantity'];
                }
                $last_customer_id = $each_order['customerNumber'];
                $last_customer_name = $each_order['customerName'];
            }

            $top_customers = $this->sortWithLimit($customer_que,'itemQuantity',10,'DESC');


            return response()->json(['status' => 200, 'response' => $top_customers]);
        }catch(\Exception $e){
            return response()->json(['status'=>500,'response'=>'Internal Server Error']);
        }
    }

    // get top 10 product having high profit
    public function topHighProfitProducts(Request $request)
    {
        try{
            $products = Products::all();
            $product_que = [];
            foreach($products as $each_product){
                $cost_price = $each_product->buyPrice;
                $selling_price = $each_product->MSRP;
                $profit_of_item = $selling_price = $cost_price;
                $product_que[] = array(
                    'productCode' => $each_product['productCode'],
                    'productName'   => $each_product['productName'],
                    'productProfit' => $profit_of_item,
                );

            }

            $top_profit_products = $this->sortWithLimit($product_que,'productProfit',10,'DESC');

            return response()->json(['status' => 200, 'response' => $top_profit_products]);
        }catch(\Exception $e){
            return response()->json(['status'=>500,'response'=>'Internal Server Error']);
        }
    }


    public function sortWithLimit($sort_array, $sort_key, $sort_limit, $sort_order = 'DESC')
    {

        $sorted_array = usort($sort_array, function($a, $b) use ($sort_order, $sort_key) {
            if($sort_order == 'DESC'){
                if($a[$sort_key]==$b[$sort_key]) return 0;
                return $a[$sort_key] < $b[$sort_key]?1:-1;
            }else{
                return $a[$sort_key] <=> $b[$sort_key];
            }
        });
        $return_data = array_slice($sort_array, 0, $sort_limit);
        return $return_data;
    }
}
