<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


use App\Models\OrderDetails;
use App\Models\Customers;

class Orders extends Model
{
    use HasFactory;

    protected $table = 'orders';

    public function order_details(){
        return $this->hasMany(OrderDetails::class,'orderNumber','orderNumber');
    }

    public function customer(){
        return $this->hasOne(Customers::class,'customerNumber','customerNumber');
    }
}
